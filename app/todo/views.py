from django.shortcuts import render
from django.http import HttpResponse
from todo.models import Todo

# Create your views here.

def index(request):
    value = ''
    success = False
    if request.method == "POST":
        t= Todo(task_name=request.POST['task'])
        t.save()
        value = request.POST['task']
        success = True
    TASKS = Todo.objects.all()
    context = {'task_list' : TASKS,'success': success, 'value' : value }   
    return render(request, 'todo_app.html', context)

def get_data(request, value):
    t = Todo.objects.filter(task_name=value)
    t.delete()
    context = {'task_list': Todo.objects.all(), 'removed': True, 'value': value}
    return render(request, 'todo_app.html', context)


def contact(request):
    return render(request, 'contact.html')


def about(request):
    return render(request, 'about_us.html')

